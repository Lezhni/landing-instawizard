$(document).ready(function() {

	var $parallaxSection = $('.block1');
	var parallaxFunc = function(){
		if ($(window).width() >= 768) {
			var offset = $parallaxSection.offset().top;
			var scrollTop = $(window).scrollTop();
			var yPos = -(offset - scrollTop)/3;
				yPos = -yPos;
			var coords = 'center '+ yPos + 'px';
			$parallaxSection.css( { backgroundPosition: coords});
		} else {
			$parallaxSection.css( {
				backgroundPosition: 'center',
			});
		}
	};
	parallaxFunc(); 

	$(window).scroll(function (){
		parallaxFunc();
	});

	$('.block1').on('scrollSpy:enter', function() {

		if (!$(this).hasClass('showed')) {
			$({someValue: 0}).animate({someValue: 90}, {
		        duration: 4000,
		        easing:'swing',
		        step: function() {
		            $('.instagram-advantages .advantage-single:first-child .advantage-count').text(Math.round(this.someValue));
		        }
		    });
		    $({someValue1: 0}).animate({someValue1: 40}, {
		        duration: 4000,
		        easing:'swing',
		        step: function() {
		            $('.instagram-advantages .advantage-single:nth-child(2) .advantage-count').text(Math.round(this.someValue1));
		        }
		    });
		    $({someValue2: 0}).animate({someValue2: 1000}, {
		        duration: 4000,
		        easing:'swing',
		        step: function() {
		            $('.instagram-advantages .advantage-single:nth-child(3) .advantage-count').text(Math.round(this.someValue2));
		        }
		    });
		    $({someValue3: 0}).animate({someValue3: 8500}, {
		        duration: 4000,
		        easing:'swing',
		        step: function() {
		            $('.instagram-advantages .advantage-single:last-child .advantage-count').text(Math.round(this.someValue3));
		        }
		    });
		}
		$(this).addClass('showed');
	});

	$('.block2 .container-content').on('scrollSpy:enter', function() {
	    $('.instagram-diff').addClass('visible');
	    var speed = 250;
	    $(".instagram-services ul:first-child").stop().animate({
	        'opacity': 1,
	        'right': 0
	    }, speed);
	    $(".instagram-services ul:last-child").stop().delay(speed).animate({
	        'opacity': 1,
	        'right': 0
	    }, speed);
	});
	$('.block2 .container-content').on('scrollSpy:exit', function() {
	    $('.instagram-diff').removeClass('visible');
	    $(".instagram-services ul").removeAttr('style');
	});

	$('.btn-popup').magnificPopup({
		type: 'inline',
		callbacks: {

			open: function() {
				$('body').css('margin-right', '0');
			}
		}
	});

	$('.block3 .instagram-heart').on('scrollSpy:enter', function() {
		$(this).addClass('visible');
	});
	$('.block3 .instagram-heart').on('scrollSpy:exit', function() {
		$(this).removeClass('visible');
	});
	$('.block3 .instagram-examples').on('scrollSpy:enter', function() {
		$(this).addClass('visible');
	});
	$('.block3 .instagram-examples').on('scrollSpy:exit', function() {
		$(this).removeClass('visible');
	});
	$('.instagram-price').on('scrollSpy:enter', function() {
		$(this).addClass('visible');
	});
	$('.instagram-price').on('scrollSpy:exit', function() {
		$(this).removeClass('visible');
	});

	$('.block1').scrollSpy();
	$('.block2 .container-content').scrollSpy();
	$('.block3 .instagram-examples').scrollSpy();
	$('.block3 .instagram-heart').scrollSpy();
	$('.instagram-price').scrollSpy();

	$('.block5').slick({
		autoplay: true,
		arrows: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		speed:1000
	});

	$('form').submit(function() {

		var name = $(this).find('input[name=name]').val();
		var email = $(this).find('input[name=email]').val();
		var instagram = $(this).find('input[name=instagram]').val();

		$.ajax({
			method: 'POST',
			url: 'send.php',
			data: {
				name: name,
				email: email,
				instagram: instagram
			},
			success: function(data) {
				if (data == 'sended') {
					$.magnificPopup.close();
					$.magnificPopup.open({
						items: {
							src: '#thanks'
						}
					});
				} else {
					alert('Ошибка отправки, попробуйте снова');
				}
			}
		})
		return false;
	});
});